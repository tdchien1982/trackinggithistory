# TrackingGitHistory

Tracking All History of codes on GitLab (for synchronizing GitLab CE on localhost)

## History (day by day)

# 13/Jul/2022
- Copy projects from Gitlab-CE (CI-CD)
    + Go Web Basic
    + React Web Basic
    + Angular Web Basic
    + Spring Web Basic
 - Copy projects from Gitlab-CE (Provisions-ENVs)
    + Linode

# 19/Jul/2022
- Create new Project
   + Deploy a WordPress Site using Linode+Terraform+StackScripts

